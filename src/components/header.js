import React, { Suspense } from "react";
const WebHeader = React.lazy(() => import('./webheader'));
const MobileHeader = React.lazy(() => import('./mbheader'));

const Header = () => {
  return (
    <div className="sticky-top">
      <div id="site-header">
        <Suspense fallback={<div id="content-desktop"></div>}>
          <WebHeader/>
        </Suspense>
        <Suspense fallback={<div id="content-mobile"></div>}>
          <MobileHeader/>
        </Suspense>
      </div>
    </div>
  );
};
export default Header;
