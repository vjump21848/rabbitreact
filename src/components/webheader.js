import React from "react";
import { NavLink } from "react-router-dom";
const WebHeader = () => (
    <nav
    className="navbar navbar-expand-lg navbar-dark sticky-top site-header"
    id="content-desktop"
  >
    <div className="container">
      <div className="row">
        <div className="col-md-auto my-auto">
          <a className="navbar-brand" href="/">
            <img
              src="../public/image/Logo.png"
              width="130"
              height="46.71"
              alt="Rabbitsstore"
            />
          </a>
        </div>
        <div className="col-md-auto ml-auto">
          <ul className="navbar-nav justify-content-end ">
            <li className="nav-item">
              <h5 className="nav-link">
                <NavLink
                  exact
                  to="/"
                  className="text-decoration-none nav-link"
                  activeClassName="active"
                >
                  หน้าแรก
                </NavLink>
              </h5>
            </li>
            <li className="nav-item">
              <h5 className="nav-link active">
                <NavLink
                  to="/DTG/"
                  className=" text-decoration-none nav-link"
                  activeClassName="active"
                >
                  DTG
                </NavLink>
              </h5>
            </li>
            <li className="nav-item">
              <h5 className="nav-link">
                <NavLink
                  to="/DST/"
                  className="text-decoration-none nav-link "
                  activeClassName="active"
                >
                  DST
                </NavLink>
              </h5>
            </li>
            <li className="nav-item">
              <h5 className="nav-link">
                <NavLink
                  to="/blockscreen/"
                  className="text-decoration-none nav-link"
                  activeClassName="active"
                >
                  Block Screen
                </NavLink>
              </h5>
            </li>
            <li className="nav-item">
              <h5 className="nav-link">
                <NavLink
                  to="/embroidery/"
                  className="text-decoration-none nav-link"
                  activeClassName="active"
                >
                  Embroidery
                </NavLink>
              </h5>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
)
export default WebHeader