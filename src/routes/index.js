import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";
import home from "../pages/home/index"
import DTG from  "../pages/DTG/index"
import DST from  "../pages/DST/index"
import Embroidery from  "../pages/embroidery/index"
import Blockscreen from  "../pages/blockscreen/index"

const Routing = () => {
  return (
      <Switch>
        <Route exact path="/" component={home} />

        <Route exact path="/DTG" component={DTG} />

        <Route exact path="/DST" component={DST} />

        <Route exact path="/blockscreen" component={Blockscreen} />

        <Route exact path="/embroidery" component={Embroidery} />
      </Switch>
  );
};
export default Routing;
