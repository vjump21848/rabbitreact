import React, { Suspense } from "react";
import { hydrate, render } from "react-dom";
import "./style.css";
import "./carousel.min.css";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter } from "react-router-dom";
import "./pages/home/home.css";
import Header  from "./components/header"
import Routing  from "./routes/index"
import Footer from "./components/footer"
import 'lazysizes';
import "./bootstrap.min.css"
const AppWithRouter = () => (
  <BrowserRouter>
    <Suspense fallback={<div>{"  "}</div>}>
      <Header />
    </Suspense>
    <Suspense fallback={<div id="lazycontent content-desktop">{"  "}</div>}>
      <Routing />
    </Suspense>
    <Suspense fallback={<div>{"  "}</div>}>
      <Footer />
    </Suspense>
  </BrowserRouter>
);
const rootElement = document.getElementById("root");
if (rootElement.hasChildNodes()) {
  hydrate(<AppWithRouter />, rootElement);
} else {
  render(<AppWithRouter />, rootElement);
}
serviceWorker.unregister();
